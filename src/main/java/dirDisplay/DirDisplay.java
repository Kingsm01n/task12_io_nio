package dirDisplay;

import MVCpattern.View;

import java.io.File;
import java.io.IOException;

public class DirDisplay {
    public void dirdisplay (){
        File dir = new File("C:\\Users\\demid\\IdeaProjects\\task12");
        File[] children = dir.listFiles();
        for (File file : children) {
            View.display(file.getAbsolutePath());
        }
    }
}
