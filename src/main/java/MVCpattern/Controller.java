package MVCpattern;

import java.io.*;

public class Controller {
    long counter = 0;
    void byteReading() throws FileNotFoundException{
        try(
            InputStream inputStream = new FileInputStream("C:\\Users\\demid\\IdeaProjects\\task12\\text.txt");
        ){
            int code = inputStream.read();
            while(code != -1){
                code = inputStream.read();
                counter++;
            }
            View.display(counter);
        } catch (IOException e) {
            e.printStackTrace();
        }
        counter = 0;
    }
    void bufferReading() throws FileNotFoundException{
        try(
                DataInputStream dataInputStream = new DataInputStream(new BufferedInputStream(new FileInputStream("C:\\Users\\demid\\IdeaProjects\\task12\\text.txt")))
        ){
            int code = dataInputStream.read();
            while(code != -1){
                code = dataInputStream.read();
                counter++;
            }
            View.display(counter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    void myImplOfPushStreamData() throws IOException{
        PrintWriter pw = new PrintWriter(System.out, true);
        String str = "Hello ";
        byte b[] = str.getBytes();
        ByteArrayInputStream bout = new ByteArrayInputStream(b);
        PushbackInputStream push = new PushbackInputStream(bout);

        // checking no. of bytes available
        pw.println("availble bytes: " + push.available());

        // checking if mark is supported
        pw.println("mark supported? :" + push.markSupported());

        pw.close();
    }
}
