package MVCpattern;

import dirDisplay.DirDisplay;

import java.io.IOException;

public class Application {
    public static void main(String[] args) throws IOException {
        Controller c = new Controller();
        DirDisplay d = new DirDisplay();
        c.byteReading();
        c.bufferReading();
        c.myImplOfPushStreamData();
        d.dirdisplay();
    }
}
